/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
typedef enum {
	CAN_XCEIVER_ENABLE,
	CAN_XCEIVER_DISABLE
} can_xceiver_status_t;

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CO2_2_RIGHT_Pin GPIO_PIN_13
#define CO2_2_RIGHT_GPIO_Port GPIOC
#define CO2_2_LEFT_Pin GPIO_PIN_14
#define CO2_2_LEFT_GPIO_Port GPIOC
#define BATT_VOLT_Pin GPIO_PIN_0
#define BATT_VOLT_GPIO_Port GPIOC
#define PURGE_POT_Pin GPIO_PIN_1
#define PURGE_POT_GPIO_Port GPIOC
#define THRUST_POT_Pin GPIO_PIN_2
#define THRUST_POT_GPIO_Port GPIOC
#define FILL_POT_Pin GPIO_PIN_3
#define FILL_POT_GPIO_Port GPIOC
#define RELIEF_POT_Pin GPIO_PIN_0
#define RELIEF_POT_GPIO_Port GPIOA
#define RT_MSP_Pin GPIO_PIN_1
#define RT_MSP_GPIO_Port GPIOA
#define CC_MSP_Pin GPIO_PIN_2
#define CC_MSP_GPIO_Port GPIOA
#define INJ_MSP_Pin GPIO_PIN_3
#define INJ_MSP_GPIO_Port GPIOA
#define RT_LC_Pin GPIO_PIN_4
#define RT_LC_GPIO_Port GPIOA
#define ENGINE_LC3_Pin GPIO_PIN_5
#define ENGINE_LC3_GPIO_Port GPIOA
#define ENGINE_LC2_Pin GPIO_PIN_6
#define ENGINE_LC2_GPIO_Port GPIOA
#define ENGINE_LC1_Pin GPIO_PIN_7
#define ENGINE_LC1_GPIO_Port GPIOA
#define QR_RIGHT_Pin GPIO_PIN_4
#define QR_RIGHT_GPIO_Port GPIOC
#define QR_LEFT_Pin GPIO_PIN_5
#define QR_LEFT_GPIO_Port GPIOC
#define TIM3_CH3_PURGE_Pin GPIO_PIN_0
#define TIM3_CH3_PURGE_GPIO_Port GPIOB
#define RELIEF_RIGHT_Pin GPIO_PIN_2
#define RELIEF_RIGHT_GPIO_Port GPIOB
#define TIM2_CH3_THRUST_Pin GPIO_PIN_10
#define TIM2_CH3_THRUST_GPIO_Port GPIOB
#define RELIEF_LEFT_Pin GPIO_PIN_12
#define RELIEF_LEFT_GPIO_Port GPIOB
#define Pyro4_Pin GPIO_PIN_13
#define Pyro4_GPIO_Port GPIOB
#define Pyro3_Pin GPIO_PIN_14
#define Pyro3_GPIO_Port GPIOB
#define Pyro2_Pin GPIO_PIN_15
#define Pyro2_GPIO_Port GPIOB
#define Pyro1_Pin GPIO_PIN_6
#define Pyro1_GPIO_Port GPIOC
#define CAN_STB_Pin GPIO_PIN_9
#define CAN_STB_GPIO_Port GPIOC
#define CAN_EN_Pin GPIO_PIN_8
#define CAN_EN_GPIO_Port GPIOA
#define CO2_1_LEFT_Pin GPIO_PIN_10
#define CO2_1_LEFT_GPIO_Port GPIOA
#define CO2_1_RIGHT_Pin GPIO_PIN_15
#define CO2_1_RIGHT_GPIO_Port GPIOA
#define DEBUG_LED_Pin GPIO_PIN_2
#define DEBUG_LED_GPIO_Port GPIOD
#define THRUST_PWM_Pin GPIO_PIN_4
#define THRUST_PWM_GPIO_Port GPIOB
#define TIM3_CH2_FILL_Pin GPIO_PIN_5
#define TIM3_CH2_FILL_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
