/*
 * MotorController_Config.h
 *
 *  Created on: Apr 30, 2021
 *      Author: george
 */

// COMMENT OUT IF IN RMC V1
#define USING_SERVO_MOTOR

#ifndef _MOTORCONTROLLER_CONFIG_H_
#define _MOTORCONTROLLER_CONFIG_H_

#include "stdint.h"

#ifndef USING_SERVO_MOTOR
#define USING_POTENTIOMETERS
#endif

#define THRUST_POTENTIOMETER_STARTING_POSITION_DEG 0
#define THRUST_POTENTIOMETER_FINAL_POSITION_DEG 1024

#define FILL_POTENTIOMETR_STARTING_POSITION_DEG 0
#define FILL_POTENTIOMETR_FINAL_POSITION_DEG 1024

#define PURGE_POTENTIOMETER_STARTING_POSITION_DEG 0
#define PURGE_POTENTIOMETER_FINAL_POSITION_DEG 1024

#define RECOVERY_POTENTIOMETER_STARTING_POSITION_DEG 0
#define RECOVERY_POTENTIOMETER_FINAL_POSITION_DEG 1024 

#define QUEUE_LEN_LoadCell 	50
#define QUEUE_LEN_MSP300 	50


// ADC Configuration

#define ADC_TOTAL_NUMBER 			12

#define ADC_BATTERY_VOLTAGE 		0
#define ADC_PURGE_POT				1
#define ADC_THRUST_POT 				2
#define ADC_FILL_POT				3
#define ADC_RELIEF_POT				4
#define ADC_RUNTANK_MSP300 			5
#define ADC_CC_MSP300 				6
#define ADC_INJ_MSP300 				7
#define ADC_RUNTANK_LC 				8
#define ADC_ENGINE_LC3 				9
#define ADC_ENGINE_LC2 				10
#define ADC_ENGINE_LC1 				11

#define LAUNCH_SWITCH_ID 0
#define ABORT_SWITCH_ID 4

#define RUNTANK_WEIGHT_ZERO_ID 	1
#define RUNTANK_WEIGHT_1_7_ID 	2
#define RUNTANK_WEIGHT_3_3_ID 	3

// CAN StdId
#define GLOBAL_ID 					0x0
#define DATA_LOGGER_ID 				0x0447
#define TELEMETRY_BOARD_ID 			0x0448
#define MOTOR_CONTROLLER_BOARD_ID 	0x0449

// CAN ExtId
#define CAN_TX_FRAME_1				0
#define CAN_TX_FRAME_2				1

// Different enums & structs

enum ProgressStatus{
	NEUTRAL_STATE,
	VAVLE_CHECK_STATE,
	FILL_STATE,
	FIRE_STATE,
	APPROACH_STATE,
	ABORT_STATE,
	READY_STATE,
};

enum MotorState {
	CLOSED,
	IN_PROGRESS,
	OPEN,
	UNKNOWN,
};

enum VariableType {
	FLOAT,
	DOUBLE,
	INT32_T,
	UINT8_T
};

typedef struct {
	enum MotorState fillMotor;
	enum MotorState qrMotor;
	enum MotorState thrustMotor;
	enum MotorState reliefMotor;
	enum MotorState purgeMotor;
}Motor_States;

typedef struct {
	volatile uint8_t launchActivated;
	volatile uint8_t purgeActivated;
	volatile uint8_t proceedToFire;
//	volatile uint8_t parachuteFlag;
}Flags;

#endif /* INC_MOTORCONTROLLER_CONFIG_H_ */
