/*
 * msp300.h
 *
 *  Created on: Aug 30, 2021
 *      Author: thodpap
 */

#ifndef INC_MSP300_H_
#define INC_MSP300_H_

#include "motorController_Config.h"
#include "data.h"


#define MSP300_MIN__VOLTAGE_VALUE 	1.25
#define MSP300_MAX_VOLTAGE_VALUE 	3
#define MSP300_AMPLIFICATION 		17.57

typedef struct {
	double runtank;
	double injector;
	double cc;

	Data mspRunTank;
	Data mspInjector;
	Data mspCombustionChamber;
}MSP300;

void initializeMsp(MSP300 *msp300);
void insertMsp(MSP300 *msp300, uint32_t runTankValue, uint32_t injectorValue, uint32_t ccValue);

double getPressure(Data *data, uint8_t type);

double getPressureFromADC(double analogValue, int type);

#endif /* INC_MSP300_H_ */
