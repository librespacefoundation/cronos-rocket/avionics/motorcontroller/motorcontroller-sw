
#ifndef POTENTIOMETER_H
#define POTENTIOMETER_H

#include "data.h"
#include "motorController_Config.h"

typedef struct {
	double thrust;
	double fill;
	double purge;
	double relief;

	Data thrustPot;
	Data fillPot;
	Data purgePot;
	Data reliefPot;
} Potentiometer;


void initializePot(Potentiometer *pot);
void insertPot(Potentiometer *pot, uint32_t thrustPot, uint32_t fillPot, uint32_t purgePot);

#endif
