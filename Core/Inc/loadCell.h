/*
 * hx711.h
 *
 *  Created on: Aug 30, 2021
 *      Author: thodpap
 */

#ifndef INC_LOADCELL_H_
#define INC_LOADCELL_H_

#include "motorController_Config.h"
#include "data.h"
#include "linear_regression.h"

#define LOAD_CELL_MAX__VOLTAGE_VALUE 		0
#define LOAD_CELL_MIN_VOLTAGE_VALUE 		3.3

#define RUN_TANK_LOAD_CELL_AMPLIFICATION	17.6
#define ENGINE_LOAD_CELL_AMPLIFICATION  	176.41

typedef struct {
	double runTank;
	double engine;
//	double engine1;
//	double engine2;
//	double engine3;

	Data runTankLoadCell;
	Data engineLoadCell1;
	Data engineLoadCell2;
	Data engineLoadCell3;
} LoadCell;

void initializeLoadCell(LoadCell *loadCell);
void insertLoadCell(LoadCell *loadCell, uint32_t runTankValue, uint32_t engineValue1, uint32_t engineValue2,uint32_t engineValue3);

//double getWeight(Data *data, uint8_t type);

double getLoadCellWeightFromAdc(double v1, double v2, double v3);
//double getWeightFromADC(double analogValue, int type);

#endif /* INC_LOADCELL_H_ */
