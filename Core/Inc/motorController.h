#ifndef INC_MOTORCONTROLLER_DATATYPES_H_
#define INC_MOTORCONTROLLER_DATATYPES_H_

#include "loadCell.h"
#include "motorController_Config.h"
#include "data.h"
#include "msp300.h"

//#include "potentiometer.h"


typedef struct{
	Flags flags;
	MSP300 msp300;
	LoadCell loadCell;
	Motor_States motor_states;

	enum ProgressStatus progress;

}MotorController_DataStoreTypeDef;

void initializeMotorControllerData();

void insertAdcValues();

int8_t dataMatrixCheck();
int8_t canSendStruct();

#endif /* INC_MOTORCONTROLLER_DATATYPES_H_ */
