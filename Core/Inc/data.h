/*
 * data.h
 *
 *  Created on: Aug 29, 2021
 *      Author: thodpap
 */

#ifndef INC_DATA_H_
#define INC_DATA_H_

#include <math.h>
#include <stdint.h>
#include <inttypes.h>

typedef struct {
	int arrayLen;
	int counter;
	uint32_t arr[1000];
	uint32_t sumEl;
	uint32_t sumSquare;
	double average;
	double std;
	double value;
}Data;

void initialize(Data *data, int len);
void insertData(Data *data, uint32_t value);
double getValue(Data *data);

void setValue(Data *data);


#endif /* INC_DATA_H_ */
