/*
 * constants.h
 *
 *  Created on: Nov 30, 2021
 *      Author: thodoris
 */

#ifndef INC_CONSTANTS_H_
#define INC_CONSTANTS_H_

#include <stdint.h>
#include <inttypes.h>


#define SERVO_DIRECTION_TOTAL_STATES 2
enum ServoDirection {
	DEG_0,
	DEG_180,
};

typedef struct {
	// LOAD CELL VALUES
	// y = a*x + b
	double 		runTankLoadCell_a;
	double 		runTankLoadCell_b;
	uint32_t 	runTankLoadCellMinAdcValue;
	uint32_t 	runTankLoadCellZeroAdcValue;
	double 		runTankLoadCellZeroWeightValue;

	double 		engine1LoadCell_a;
	double 		engine1LoadCell_b;
	uint32_t 	engine1LoadCellMinAdcValue;
	uint32_t 	engine1LoadCellZeroAdcValue;
	double 		engine1LoadCellZeroWeightValue;


	double 		engine2LoadCell_a;
	double 		engine2LoadCell_b;
	uint32_t 	engine2LoadCellMinAdcValue;
	uint32_t 	engine2LoadCellZeroAdcValue;
	double 		engine2LoadCellZeroWeightValue;

	double 		engine3LoadCell_a;
	double 		engine3LoadCell_b;
	uint32_t 	engine3LoadCellMinAdcValue;
	uint32_t 	engine3LoadCellZeroAdcValue;
	double 		engine3LoadCellZeroWeightValue;

	// MSP300
	int 		runTankMaxPressure;
	uint32_t 	runTankMspMinAdcValue;
	uint32_t 	runTankMspMaxAdcValue;

	int 		injectorMaxPressure;
	uint32_t 	injectorMspMinAdcValue;
	uint32_t 	injectorMspMaxAdcValue;

	int 		ccMaxPressure;
	uint32_t 	ccMspMinAdcValue;
	uint32_t 	ccMspMaxAdcValue;

	// Thrust Motor

	// Servo
	uint16_t thrustServo[SERVO_DIRECTION_TOTAL_STATES];
	uint16_t fillServo[SERVO_DIRECTION_TOTAL_STATES];
	uint16_t purgeServo[SERVO_DIRECTION_TOTAL_STATES];

	uint32_t 	reliefTotalOpeningTimer; // in ms

} Constants;


void initializeConstants();


#endif /* INC_CONSTANTS_H_ */
