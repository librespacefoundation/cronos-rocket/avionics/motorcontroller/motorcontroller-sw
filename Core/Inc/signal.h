/*
 * signal.hpp
 */

#ifndef _SIGNAL_HPP_
#define _SIGNAL_HPP_ 

#include "stm32l4xx_hal.h"
#include "motorController.h"
#include "cmsis_os.h"

#include "stm32l4xx_it.h"
#include "constants.h"
#include "main.h"

#include <stdio.h>

enum Motors{
	FILL,
	THRUST,
	PURGE,
	QR,
	RELIEF,
};
enum Rotation {
	LEFT,
	RIGHT
};


void setGPIOpin(GPIO_TypeDef* GPIOx, uint16_t GPIO_pin);
void resetGPIOpin(GPIO_TypeDef* GPIOx, uint16_t GPIO_pin); 

void openMotor(enum Motors motor, enum MotorState state, enum Rotation rotation );
void closeMotor(enum Motors motor, enum MotorState state);

void moveServo(enum Motors motor, enum ServoDirection direction);

void neutralState();
void valveCheckState();
void fillState();
void fireState();
void neutralState();
void approachState();
void abortState();


// debugging functions
void onlyOpenThrust();
void onlyCloseThrust();
void onlyOpenPurge();
void onlyClosePurge();
void onlyOpenFill();
void onlyCloseFill();

#endif 
