/*
 * linear_regression.h
 *
 *  Created on: Dec 15, 2022
 *      Author: thodoris
 */

#ifndef INC_LINEAR_REGRESSION_H_
#define INC_LINEAR_REGRESSION_H_

#include <stdint.h>

#include "stm32l4xx.h"
#include "stm32l4xx_hal_flash.h"

void linear_regression_init();
void update_y(int index, double y_i);
void calculate_theta();

double calculate_runtank_weight(double adc);

#endif /* INC_LINEAR_REGRESSION_H_ */
