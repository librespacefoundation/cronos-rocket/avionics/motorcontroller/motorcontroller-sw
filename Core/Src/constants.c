/*
 * constants.c
 *
 *  Created on: Nov 30, 2021
 *      Author: thodoris
 */


#include "constants.h"

extern Constants constants;
void initializeConstants() {
	/* Sensors Data */

	// MSP
	constants.runTankMspMinAdcValue = 1513;
	constants.injectorMspMinAdcValue = 1514;
	constants.ccMspMinAdcValue = 1551;

	constants.runTankMspMaxAdcValue = 3996;
	constants.injectorMspMaxAdcValue = 4045;
	constants.ccMspMaxAdcValue = 4033;

	constants.runTankMaxPressure = 69; // bar
	constants.injectorMaxPressure = 69;
	constants.ccMaxPressure = 69;

	// Load cell line values
	constants.runTankLoadCell_a = 0.00881467;
	constants.runTankLoadCell_b = -0.0001673;
	constants.runTankLoadCellZeroAdcValue = 1969;
	constants.runTankLoadCellZeroWeightValue = 1969;

	constants.engine1LoadCell_a = 0.077;
	constants.engine1LoadCell_b = -116.65;
	constants.engine1LoadCellZeroAdcValue = 1523;

	constants.engine2LoadCell_a = 0.066;
	constants.engine2LoadCell_b = -102.70;
	constants.engine2LoadCellZeroAdcValue = 1550;

	constants.engine3LoadCell_a = 0.072;
	constants.engine3LoadCell_b = -109.14;
	constants.engine3LoadCellZeroAdcValue = 1520;

	/* Motor / Servo stats */

	constants.thrustServo[DEG_0] = 1250;
	constants.thrustServo[DEG_180] = 250;

	constants.fillServo[DEG_0] = 1250;
	constants.fillServo[DEG_180] = 250;

	constants.purgeServo[DEG_0] = 1250;
	constants.purgeServo[DEG_180] = 250;

	constants.reliefTotalOpeningTimer = 3300; // 3800 / 1.8;
}
