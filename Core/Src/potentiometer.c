/*
 * potentiometer.c
 *
 *  Created on: Nov 29, 2021
 *      Author: thodoris
 */

#include "potentiometer.h"

void initializePot(Potentiometer *pot) {
	initialize(&pot->thrustPot, 3);
	initialize(&pot->fillPot, 3);
	initialize(&pot->purgePot, 3);

	pot->thrust = 0.0;
	pot->fill = 0.0;
	pot->purge = 0.0;
}

void insertPot(Potentiometer *pot, uint32_t thrustPot, uint32_t fillPot, uint32_t purgePot) {
	insertData(&pot->thrustPot, thrustPot);
	insertData(&pot->fillPot, fillPot);
	insertData(&pot->purgePot, purgePot);

	pot->thrust = pot->thrustPot.value;
	pot->fill = pot->fillPot.value;
	pot->purge = pot->purgePot.value;
}


