/*
 * msp300.c
 *
 *  Created on: Aug 30, 2021
 *      Author: thodpap
 */

#include <math.h>
#include "msp300.h"
#include "constants.h"

extern Constants constants;

void initializeMsp(MSP300 *msp300) {
	initialize(&msp300->mspRunTank, QUEUE_LEN_MSP300);
	initialize(&msp300->mspInjector, QUEUE_LEN_MSP300);
	initialize(&msp300->mspCombustionChamber, QUEUE_LEN_MSP300);

	msp300->runtank = 0.0;
	msp300->injector = 0.0;
	msp300->cc = 0.0;
}
void insertMsp(MSP300 *msp300, uint32_t runTankValue, uint32_t injectorValue, uint32_t ccValue) {
	insertData(&msp300->mspRunTank, runTankValue);
	insertData(&msp300->mspInjector, injectorValue);
	insertData(&msp300->mspCombustionChamber, ccValue);

	msp300->runtank = getPressure(&msp300->mspRunTank, ADC_RUNTANK_MSP300);
	msp300->injector = getPressure(&msp300->mspInjector, ADC_INJ_MSP300);
	msp300->cc = getPressure(&msp300->mspCombustionChamber, ADC_CC_MSP300);
}

double getPressure(Data *data, uint8_t type) {
	return getPressureFromADC(data->value, type);
}

double getPressureFromADC(double analogValue, int type) {
	uint32_t max = 0;
	uint32_t min = 0;
	int max_pressure = 0;

	switch(type) {

	case ADC_RUNTANK_MSP300:
		max = constants.runTankMspMaxAdcValue;
		min = constants.runTankMspMinAdcValue;
		max_pressure = constants.runTankMaxPressure;

		break;

	case ADC_INJ_MSP300:
		max = constants.injectorMspMaxAdcValue;
		min = constants.injectorMspMinAdcValue;
		max_pressure = constants.injectorMaxPressure;

		break;

	case ADC_CC_MSP300:
		max = constants.ccMspMaxAdcValue;
		min = constants.ccMspMinAdcValue;
		max_pressure = constants.ccMaxPressure;

		break;

	default:
		return -1.0;
	}

	if (analogValue > (double) max)
		analogValue = (double) max;
	if (analogValue < (double) min)
		analogValue = (double) min;


	return 1.0 + (double)(analogValue - (double)min) / (double)(max - min) * max_pressure;
}
