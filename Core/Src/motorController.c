/*
 * MotorController_Datatypes.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cmsis_os.h"
#include "main.h"
#include "motorController.h"


extern MotorController_DataStoreTypeDef motorControllerData;
extern uint32_t adcValuesChannel1[ADC_TOTAL_NUMBER];
extern CAN_HandleTypeDef hcan1;

static int8_t neutralStateCheck();

static int8_t neutralState_();
static int8_t valveStateCheck();
static int8_t fillStateCheck();
static int8_t fireStateCheck();
static int8_t approachStateCheck();
static int8_t abortState();
static int8_t readyState();

void initializeMotorControllerData(){
	motorControllerData.flags.launchActivated = 0;
	motorControllerData.flags.purgeActivated = 0;

	initializeMsp(&motorControllerData.msp300);
	initializeLoadCell(&motorControllerData.loadCell);

	motorControllerData.motor_states.fillMotor = CLOSED; // will be fixed dynamically in valve check
	motorControllerData.motor_states.purgeMotor = CLOSED;
	motorControllerData.motor_states.thrustMotor = CLOSED;

	motorControllerData.motor_states.qrMotor = CLOSED;
	motorControllerData.motor_states.reliefMotor = CLOSED;

	motorControllerData.progress = NEUTRAL_STATE;
}

void insertAdcValues() {
	insertMsp(&motorControllerData.msp300,
			adcValuesChannel1[ADC_RUNTANK_MSP300],
			adcValuesChannel1[ADC_INJ_MSP300],
			adcValuesChannel1[ADC_CC_MSP300]
	);

	insertLoadCell(&motorControllerData.loadCell,
			adcValuesChannel1[ADC_RUNTANK_LC],
			adcValuesChannel1[ADC_ENGINE_LC1],
			adcValuesChannel1[ADC_ENGINE_LC2],
			adcValuesChannel1[ADC_ENGINE_LC3]);
}

int8_t dataMatrixCheck(){
	// flags are on motorControllerData.flags
	switch(motorControllerData.progress){
	case NEUTRAL_STATE:
		return neutralState_();

	case VAVLE_CHECK_STATE:
		return valveStateCheck();

	case FILL_STATE:
		return fillStateCheck();

	case FIRE_STATE:
		return fireStateCheck();

	case APPROACH_STATE:
		return approachStateCheck();

	case ABORT_STATE:
		return abortState();

	case READY_STATE:
		return readyState();

	default:
		break;
	}

	return 0;
}

static int8_t neutralStateCheck() {
	if (!(motorControllerData.msp300.runtank <= 2.0))
		return 0;

	if (!(motorControllerData.msp300.injector <= 2.0))
		return 0;

	if (!(motorControllerData.msp300.cc <= 2.0))
		return 0;

	if (motorControllerData.loadCell.runTank >= 6.0)
		return 0;

	return 1;
}
static int8_t neutralState_() {
	return 1;
}
static int8_t valveStateCheck() {
	return neutralStateCheck();
}
int8_t fillStateCheck() {
	if (!(motorControllerData.msp300.runtank <= 65.0))
		return 0;

	if (!(motorControllerData.msp300.injector <= 2.0))
		return 0;

	if (!(motorControllerData.msp300.cc <= 2.0))
		return 0;


	if (!(motorControllerData.loadCell.runTank <= 6.0))
		return 0;

	return 1;
}
static int8_t fireStateCheck() {
	if (!(motorControllerData.msp300.runtank <= 65.0))
			return 0;
	if (!(motorControllerData.msp300.injector <= 65.0))
		return 0;

	if (!(motorControllerData.msp300.cc <= 45.0))
		return 0;

	if (!(motorControllerData.loadCell.runTank <= 6.0))
		return 0;

	return 1;
}
static int8_t approachStateCheck() {
	return neutralStateCheck();
}
static int8_t abortState() {
	return 1;
}

static int8_t readyState() {
	return neutralStateCheck();
}


/* Retrieve data from can bus msges */
//double doubleFromBytes(uint8_t *buffer) {
//    double result;
//    memcpy(&result, buffer, sizeof(double));
//    return result;
//}
//uint32_t largeFromBytes(uint8_t *buffer) {
//	return buffer[0] | (buffer[1] << 8) | (buffer[2] << 16) | (buffer[3] << 24);
//}

int8_t canSendStruct()
{
	CAN_TxHeaderTypeDef   TxHeader;
	static uint32_t       TxMailbox;

	uint8_t data[8];
	memset(data, 0, 8);

	TxHeader.IDE = CAN_ID_STD;
	TxHeader.RTR = CAN_RTR_DATA;
	TxHeader.TransmitGlobalTime = DISABLE;


	TxHeader.DLC = 8;
	TxHeader.StdId = (GLOBAL_ID << 4) | CAN_TX_FRAME_1;


	/*
	 * 1st data: 		1				2			3		4					5,6							7,8
	 */

	int i = 0;
	uint16_t value = motorControllerData.loadCell.engine * 100;
	data[i++] = (uint8_t)((value >> 8) & 0xff );
	data[i++] = (uint8_t)(value & 0xff);
	i++;

	uint16_t temp = motorControllerData.msp300.runtank * 100; // 2 bytes runtank pressure (keep 2 floating points)
	data[i++] = (temp >> 8) & 255;
	data[i++] = (temp & 255); // 255 = 0b11111111

	temp = motorControllerData.msp300.injector * 100;
	data[i++] = (temp >> 8) & 255;
	data[i++] = (temp & 255);

	data[i++] = ((uint8_t)(motorControllerData.motor_states.thrustMotor & 0xf) << 6)
				| ((uint8_t)(motorControllerData.motor_states.fillMotor & 0xf) << 4)
				| ((uint8_t)(motorControllerData.motor_states.purgeMotor & 0xf) << 2)
				| ((uint8_t)(motorControllerData.motor_states.reliefMotor & 0xf));

	if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, (uint8_t *)data, &TxMailbox) != HAL_OK) {
//		Error_Handler();
//		return -1;
	}

	/*
	 * 2nd data:
	 */

	i = 0;
	TxHeader.DLC = 8;
	TxHeader.StdId = (GLOBAL_ID << 4) | CAN_TX_FRAME_2;

	temp = motorControllerData.msp300.cc * 100;
	data[i++] = (temp >> 8) & 255;
	data[i++] = (temp & 255);


	temp = (int16_t)(100 * motorControllerData.loadCell.runTank);
	data[i++] = (temp >> 8) & 255;
	data[i++] = (temp & 255);

	data[i++] = (((uint8_t)motorControllerData.progress & 0xff) << 4)
			| (uint8_t)(motorControllerData.motor_states.qrMotor & 0xf);

	data[i++] = (uint8_t)((adcValuesChannel1[ADC_PURGE_POT] >> 4) & 0xff);
	data[i++] = (uint8_t)((adcValuesChannel1[ADC_THRUST_POT] >> 4) & 0xff);
	data[i++] = (uint8_t)((adcValuesChannel1[ADC_FILL_POT] >> 4) & 0xff);

	if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, (uint8_t *)data, &TxMailbox) != HAL_OK) {
//		Error_Handler();
//		return -1;
	}

	while (HAL_CAN_IsTxMessagePending(&hcan1, TxMailbox)) {} // wait to empty mailboxes


	return 0;
}

