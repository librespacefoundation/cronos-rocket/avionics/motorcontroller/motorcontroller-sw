/*
 * data.c
 *
 *  Created on: Aug 29, 2021
 *      Author: thodpap
 */

#include "data.h"
#include <string.h>
#include <stdlib.h>

void initialize(Data *data, int len) {
	if (len <= 0) {
		len = 1;
	}

	data->arrayLen = len;
	memset(data->arr, 0, len);

	data->counter = 0;
	data->average = 0.0;
	data->std = 0.0;
	data->sumEl = 0;
	data->sumSquare = 0;
	data->value = 0.0;
}

/*
 * 0 0 0 0 0 (len = 5)
 * 1 ->
 * 1 0 0 0 0
 * sumEl = 1
 * sum^2 = 1
 * average = 1 / 5
 * std = sqrt(1 / 5 - 2 * (1/5) * 1 / 5 +  (1/5)^2) = sqrt(1/5 - 2/25 + 1/25) = sqrt(1/5 - 1/25) = sqrt(4/25) = 2/5
 *
 * (1 - 1/5)^2 + 4 * (1/5)^2 = (4/5)^2 + 4/25 = 16/25 + 4/25 = 20/25
 * 4 / (25)
 * std = sqrt(4/25) = 2/5 = stdCalc
 *
 * 2 ->
 * 1 2 0 0 0
 * sumEl = 3
 * sum^2 = 5
 * average = 3/5
 * std = sqrt( 5/5 - 2*(3/5)*3/5 + (3/5)^2) = sqrt(1 - 2*9/25 + 9/25) = sqrt(1 - 9/25) = sqrt(16/25) = 4/5
 *
 * (1-3/5)^2 + (2-3/5)^2 + 3 * (3/5)^2 = 4/25 + 49/25 + 3 * 9/25 = (4 + 49 + 27)/25 = 80/25
 * 80/25 * 1/5 = 16/25
 *  sqrt(16/25) = 4/5
 *
 *  1 2 3 4 5 -> 1^2 + 2^2 + 3^3 + 4^2 + 5^2
 *
 *  sum (x- μ) = sum (x^2 - 2xμ + μ^2) = sum(x^2) - 2μ * sum(x) + μ^2 = sum_squared - 2μ * sum + μ^2
 */
void insertData(Data *data, uint32_t value) {
	data->sumEl += value - data->arr[data->counter]; // sliding window - new value arrives and old is dismissed
	data->average = (double) data->sumEl / data->arrayLen;

	data->sumSquare += value * value - data->arr[data->counter] * data->arr[data->counter];

	data->std = sqrt((double)data->sumSquare / data->arrayLen - 2.0 * data->average * data->sumEl / data->arrayLen + (double)data->average * data->average);

	data->arr[data->counter++] = value; // first update then change counter
	data->counter %= data->arrayLen; 	// cyclic changes

	setValue(data);
}

double getValue(Data *data) {
	return data->value;
}


void setValue(Data *data) {
	int size = 0;
	uint32_t sumElInterior = 0;


	for (int i = 0; i < data->arrayLen; ++i) {
		if ( (data->average - data->std) <= data->arr[i] && data->arr[i] <= (data->average + data->std)) {
			++size;
			sumElInterior += data->arr[i];
		}
	}
	if (size == 0) {
		data->value = data->average;
	} else {
		data->value = (double)sumElInterior / (double)size; // mean value of adc values
	}

}

