/*
 * hx711.c
 *
 *  Created on: Aug 30, 2021
 *      Author: thodpap
 */

#include "loadCell.h"
#include "constants.h"
#include <math.h>

extern Constants constants;

void initializeLoadCell(LoadCell *loadCell) {
	initialize(&loadCell->runTankLoadCell, QUEUE_LEN_LoadCell);
	initialize(&loadCell->engineLoadCell1, QUEUE_LEN_LoadCell);
	initialize(&loadCell->engineLoadCell2, QUEUE_LEN_LoadCell);
	initialize(&loadCell->engineLoadCell3, QUEUE_LEN_LoadCell);

	loadCell->runTank = 0.0;
	loadCell->engine = 0.0;
}
void insertLoadCell(LoadCell *loadCell, uint32_t runTankValue, uint32_t engineValue1, uint32_t engineValue2, uint32_t engineValue3) {
	insertData(&loadCell->runTankLoadCell, runTankValue);
	insertData(&loadCell->engineLoadCell1, engineValue1);
	insertData(&loadCell->engineLoadCell2, engineValue2);
	insertData(&loadCell->engineLoadCell3, engineValue3);

	loadCell->runTank = calculate_runtank_weight(loadCell->runTankLoadCell.value);

	loadCell->engine = getLoadCellWeightFromAdc(
			loadCell->engineLoadCell1.value,
			loadCell->engineLoadCell2.value,
			loadCell->engineLoadCell3.value
	);
}


double getLoadCellWeightFromAdc(double v1, double v2, double v3) {
	return (v1 + v2 + v3 - 4630)/ 12.93;
}
//double getWeight(Data *data, uint8_t type) {
//	return getWeightFromADC(data->value, type);
//}

//static double getWeightFromADC(double analogValue, int type) {
//	double a;
//	double b;
//
//	switch(type) {
////	0,728487147
//	case ADC_RUNTANK_LC:
////		a = constants.runTankLoadCell_a;
////		b = constants.runTankLoadCell_b - constants.runTankLoadCellZeroWeightValue;
//		return (constants.runTankLoadCell_a) * (analogValue - constants.runTankLoadCellZeroWeightValue) + constants.runTankLoadCell_b;
//		break;
//
//	case ADC_ENGINE_LC1:
//		a = constants.engine1LoadCell_a;
//		b = constants.engine1LoadCell_b - constants.engine1LoadCellZeroWeightValue;
//
//		break;
//
//	case ADC_ENGINE_LC2:
//		a = constants.engine2LoadCell_a;
//		b = constants.engine2LoadCell_b - constants.engine2LoadCellZeroWeightValue;
//
//		break;
//
//	case ADC_ENGINE_LC3:
//		a = constants.engine3LoadCell_a;
//		b = constants.engine3LoadCell_b - constants.engine3LoadCellZeroWeightValue;
//
//		break;
//	default:
//		return -1.0;
//	}
//
//	double res = a * analogValue + b;
//
//	if (res < 0.0)
//		res = 0.0;
//
//	return res;
//}
