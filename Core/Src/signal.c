/*
 * signals.cpp
 */

#include "signal.h"

extern MotorController_DataStoreTypeDef motorControllerData;
extern Constants constants;

extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim8;

void setGPIOpin(GPIO_TypeDef* GPIOx, uint16_t GPIO_pin){
	HAL_GPIO_WritePin(GPIOx, GPIO_pin, GPIO_PIN_SET);
}
void resetGPIOpin(GPIO_TypeDef* GPIOx, uint16_t GPIO_pin) {
	HAL_GPIO_WritePin(GPIOx, GPIO_pin, GPIO_PIN_RESET);
}


/*
 * Rotation:
 * 0 : go Left
 * 1 : go Right
 * else: close motor and unknown state
 */
void openMotor(enum Motors motor, enum MotorState state, enum Rotation rotation) {
	switch(motor) {
	case FILL:
	case THRUST:
	case PURGE:
		break;

	case QR:
		switch(rotation) {
		case RIGHT:
			resetGPIOpin(QR_LEFT_GPIO_Port, QR_LEFT_Pin);
			setGPIOpin(QR_RIGHT_GPIO_Port, QR_RIGHT_Pin);
			break;
		case LEFT:
			setGPIOpin(QR_LEFT_GPIO_Port, QR_LEFT_Pin);
			resetGPIOpin(QR_RIGHT_GPIO_Port, QR_RIGHT_Pin);
			break;
		default:
			closeMotor(motor, UNKNOWN);
			break;
		}
		motorControllerData.motor_states.qrMotor = state;
		break;
		break;
	case RELIEF:
		switch(rotation) {
		case RIGHT:
			resetGPIOpin(RELIEF_LEFT_GPIO_Port, RELIEF_LEFT_Pin);
			setGPIOpin(RELIEF_RIGHT_GPIO_Port, RELIEF_RIGHT_Pin);
			break;
		case LEFT:
			setGPIOpin(RELIEF_LEFT_GPIO_Port, RELIEF_LEFT_Pin);
			resetGPIOpin(RELIEF_RIGHT_GPIO_Port, RELIEF_RIGHT_Pin);
			break;
		default:
			closeMotor(motor, UNKNOWN);
			break;
		}
		motorControllerData.motor_states.reliefMotor = OPEN;
		break;
	}
}

void closeMotor(enum Motors motor, enum MotorState state) {
	switch(motor) {
	case FILL:
	case THRUST:
	case PURGE:
		break;

	case QR:
		resetGPIOpin(QR_LEFT_GPIO_Port, QR_LEFT_Pin);
		resetGPIOpin(QR_RIGHT_GPIO_Port, QR_RIGHT_Pin);
		motorControllerData.motor_states.qrMotor = state;
		break;
	case RELIEF:
		resetGPIOpin(RELIEF_LEFT_GPIO_Port, RELIEF_LEFT_Pin);
		resetGPIOpin(RELIEF_RIGHT_GPIO_Port, RELIEF_RIGHT_Pin);
		motorControllerData.motor_states.reliefMotor = state;
		break;
	}
}

void moveServo(enum Motors motor, enum ServoDirection direction) {
	switch(motor) {
	case THRUST:
		htim2.Instance->CCR3 = constants.thrustServo[direction]; // 250 + (int)(50/9 * degrees);
		break;
	case FILL:
		htim3.Instance->CCR2 = constants.fillServo[direction];
		break;
	case PURGE:
		htim3.Instance->CCR3 = constants.purgeServo[direction];
		break;

	case QR:
	case RELIEF:
		break;
	}
}
void neutralState() {
	HAL_GPIO_TogglePin(CO2_2_RIGHT_GPIO_Port, CO2_2_RIGHT_Pin);
	HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
	osDelay(2000);

	motorControllerData.progress = VAVLE_CHECK_STATE;
}

void valveCheckState() {
	HAL_GPIO_TogglePin(CO2_2_RIGHT_GPIO_Port, CO2_2_RIGHT_Pin);
	HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);


	// Getting motors to zero - it shouldn't do anything but it's there for safety reasons

	motorControllerData.motor_states.thrustMotor = IN_PROGRESS;
	motorControllerData.motor_states.fillMotor = IN_PROGRESS;
	motorControllerData.motor_states.purgeMotor = IN_PROGRESS;

	moveServo(FILL, DEG_0);
	moveServo(THRUST, DEG_0);
	moveServo(PURGE, DEG_0);

	osDelay(500);

	motorControllerData.motor_states.thrustMotor = CLOSED;
	motorControllerData.motor_states.fillMotor = CLOSED;
	motorControllerData.motor_states.purgeMotor = CLOSED;

	motorControllerData.progress = FILL_STATE;
}

void fillState() {
	HAL_GPIO_TogglePin(CO2_2_RIGHT_GPIO_Port, CO2_2_RIGHT_Pin);
	HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
	if (motorControllerData.flags.purgeActivated == 1) {
	  motorControllerData.progress = ABORT_STATE;
	  return;
	}

	// Open fill motor
	motorControllerData.motor_states.fillMotor = IN_PROGRESS;

	moveServo(FILL, DEG_180);
	osDelay(500);
	motorControllerData.motor_states.fillMotor = OPEN;

	// wait till run tank is filled
	while (motorControllerData.loadCell.runTank < 3.5) {
		if (motorControllerData.flags.purgeActivated == 1) {
		  motorControllerData.progress = ABORT_STATE;
		  return;
		}

		if (motorControllerData.flags.proceedToFire == 1) {
			motorControllerData.flags.proceedToFire = 0;
			break;
		}
		osDelay(5);
	}

	if (motorControllerData.flags.purgeActivated == 1) {
	  motorControllerData.progress = ABORT_STATE;
	  return;
	}

	// open QR
	openMotor(QR, IN_PROGRESS, RIGHT);

	motorControllerData.motor_states.fillMotor = IN_PROGRESS;
	moveServo(FILL, DEG_0);


	osDelay(500);

	closeMotor(QR, OPEN);
	motorControllerData.motor_states.fillMotor = CLOSED;

	// move on next state
	motorControllerData.progress = FIRE_STATE;
}


void fireState() {
//	HAL_GPIO_TogglePin(CO2_2_RIGHT_GPIO_Port, CO2_2_RIGHT_Pin);
	HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
	if (motorControllerData.flags.purgeActivated == 1) {
	  motorControllerData.progress = ABORT_STATE;
	  return;
	}

	setGPIOpin(Pyro2_GPIO_Port, Pyro2_Pin); // Open pyro 2
	setGPIOpin(Pyro3_GPIO_Port, Pyro3_Pin); // Open pyro 3
	setGPIOpin(Pyro4_GPIO_Port, Pyro4_Pin); // Open pyro 4

	uint32_t timer_now = HAL_GetTick(); // we wait 2 seconds for pyro closure and another 3 for thrust opening
	while (HAL_GetTick() - timer_now <= 1000) {
		if (motorControllerData.flags.purgeActivated == 1) {
		  resetGPIOpin(Pyro2_GPIO_Port, Pyro2_Pin); // Close pyro 2
		  resetGPIOpin(Pyro3_GPIO_Port, Pyro3_Pin); // Close pyro 3
		  resetGPIOpin(Pyro4_GPIO_Port, Pyro4_Pin); // Open pyro 4
		  motorControllerData.progress = ABORT_STATE;
		  return;
		}
		osDelay(10);
	}
	resetGPIOpin(Pyro2_GPIO_Port, Pyro2_Pin); // Close pyro 2
	resetGPIOpin(Pyro3_GPIO_Port, Pyro3_Pin); // Close pyro 3
	resetGPIOpin(Pyro4_GPIO_Port, Pyro4_Pin); // Close pyro 4

	timer_now = HAL_GetTick();
	while (HAL_GetTick() - timer_now <= 1500) {
		if (motorControllerData.flags.purgeActivated == 1) {
		  motorControllerData.progress = ABORT_STATE;
		  return;
		}
		osDelay(10);
	}


	if (motorControllerData.flags.purgeActivated == 1) {
	  motorControllerData.progress = ABORT_STATE;
	  return;
	}

	motorControllerData.motor_states.thrustMotor = IN_PROGRESS;
	moveServo(THRUST, DEG_180);
	motorControllerData.motor_states.thrustMotor = OPEN;

	osDelay(500);

	while (motorControllerData.msp300.runtank > 1.2
			|| motorControllerData.msp300.injector > 1.2
			|| motorControllerData.msp300.cc > 1.2) { // while invalid data

		  if (motorControllerData.flags.purgeActivated == 1) {
			motorControllerData.progress = ABORT_STATE;
			return;
		  }
		  osDelay(5);
	}

	osDelay(500); // for debug

	motorControllerData.progress = APPROACH_STATE;
}

void approachState() {
	HAL_GPIO_TogglePin(CO2_2_RIGHT_GPIO_Port, CO2_2_RIGHT_Pin);
	HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
	openMotor(RELIEF, IN_PROGRESS, LEFT);

	uint32_t count = HAL_GetTick();

	motorControllerData.motor_states.fillMotor = IN_PROGRESS;
	motorControllerData.motor_states.purgeMotor = IN_PROGRESS;
	motorControllerData.motor_states.thrustMotor = IN_PROGRESS;

	moveServo(FILL, DEG_180);
	moveServo(THRUST, DEG_180);
	moveServo(PURGE, DEG_180);

	osDelay(500);

	motorControllerData.motor_states.fillMotor = OPEN;
	motorControllerData.motor_states.purgeMotor = OPEN;
	motorControllerData.motor_states.thrustMotor = OPEN;

	count = HAL_GetTick() - count;
	if (count < constants.reliefTotalOpeningTimer)
		osDelay(constants.reliefTotalOpeningTimer - count);

	closeMotor(RELIEF, OPEN);

	osDelay(1000);

	motorControllerData.progress = READY_STATE;
}
void abortState() {

	HAL_GPIO_TogglePin(CO2_2_RIGHT_GPIO_Port, CO2_2_RIGHT_Pin);
	HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
	openMotor(RELIEF, IN_PROGRESS, LEFT);

	uint32_t count = HAL_GetTick();

	motorControllerData.motor_states.fillMotor = IN_PROGRESS;
	motorControllerData.motor_states.purgeMotor = IN_PROGRESS;
	motorControllerData.motor_states.thrustMotor = IN_PROGRESS;

	moveServo(FILL, DEG_180);
	moveServo(THRUST, DEG_180);
	moveServo(PURGE, DEG_180);

	osDelay(500);

	motorControllerData.motor_states.fillMotor = OPEN;
	motorControllerData.motor_states.purgeMotor = OPEN;
	motorControllerData.motor_states.thrustMotor = OPEN;

	count = HAL_GetTick() - count;
	if (count < constants.reliefTotalOpeningTimer)
		osDelay(constants.reliefTotalOpeningTimer - count);

	closeMotor(RELIEF, OPEN);

	osDelay(1000);

	motorControllerData.progress = READY_STATE;
}


// Debugging functions
void onlyOpenThrust() {
	motorControllerData.motor_states.thrustMotor = IN_PROGRESS;

	moveServo(THRUST, DEG_180);
	osDelay(500);

	motorControllerData.motor_states.thrustMotor = OPEN;
}
void onlyCloseThrust() {
	motorControllerData.motor_states.thrustMotor = IN_PROGRESS;
	moveServo(THRUST, DEG_0);
	osDelay(500);

	motorControllerData.motor_states.thrustMotor = CLOSED;
}
void onlyOpenPurge() {
	moveServo(PURGE, DEG_180);
	osDelay(500);
	motorControllerData.motor_states.purgeMotor = OPEN;

}
void onlyClosePurge() {
	moveServo(PURGE, DEG_0);
	osDelay(500);
	motorControllerData.motor_states.purgeMotor = CLOSED;
}
void onlyOpenFill() {
	moveServo(FILL, DEG_180);
	osDelay(500);

	motorControllerData.motor_states.fillMotor = OPEN;
}
void onlyCloseFill() {
	moveServo(FILL, DEG_0);
	osDelay(500);
	motorControllerData.motor_states.fillMotor = CLOSED;
}
