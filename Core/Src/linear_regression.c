/*
 * linear_regression.c
 *
 *  Created on: Dec 15, 2022
 *      Author: thodoris
 */


#include "linear_regression.h"

#define THETA_0_ADDRESS 0x08080000
#define THETA_1_ADDRESS 0x08080800

static double theta_[2][3] = {
	{0.84225901, 0.32424537, -0.16650438},
	{-0.30293195, 0.0054095, 0.29752245},
};

// 1394.1285296
// 68.9711135
static double theta[2] = {0.0, 0.0};

static double y_T[3] = {0.0, 0.0, 0.0};


void linear_regression_init() {
	volatile int64_t *addr = THETA_0_ADDRESS;
	volatile int64_t *addr1 = THETA_1_ADDRESS;

	theta[0] = (*addr)/ 10000.0;
	theta[1] = (*addr1) / 10000.0;
}
void update_y(int index, double y_i) {
	y_T[index] = y_i;
}
void calculate_theta() {
	for (int i = 0; i < 2; ++i) {
		theta[i] = 0.0;
		for (int j = 0; j < 3; ++j) {
			theta[i] += theta_[i][j] * y_T[j];
		}
	}

	// Write a uint32_t value 0x1235 to address 0x08007C00
	int64_t a = theta[0] * 10000;
	int64_t b = theta[1] * 10000;

	// Unlock Flash
	HAL_FLASH_Unlock();

	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_ALL_ERRORS);

	FLASH_EraseInitTypeDef pEraseInit;
	pEraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
	pEraseInit.Banks = FLASH_BANK_2;
	pEraseInit.Page = 256;
	pEraseInit.NbPages = 1;

	uint32_t pageError;
	HAL_FLASHEx_Erase(&pEraseInit, &pageError);

	pEraseInit.Page = 257;
	HAL_FLASHEx_Erase(&pEraseInit, &pageError);

	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_ALL_ERRORS);

	FLASH->SR = 0x00000000;
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, THETA_0_ADDRESS, a);

	FLASH->SR = 0x00000000;
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, THETA_1_ADDRESS, b);

	// Lock Flash
	HAL_FLASH_Lock();
}

double calculate_runtank_weight(double adc) {
	return (adc - theta[0]) / theta[1];
}
