# MotorController-sw

Rocket Cotor Controller (RMC) is the main avionics board for Cronos rocket, and is mainly responsible for the rocket's motor operation. RMC provides the motor's ignition, operates the valve opening mechanisms and monitors the motor's pressure. It can terminate the motor's operation automatically if it detects problematic conditions. Furthermore, RMC can amplify and read 4 load cell **ADC inputs** for measuring the engine's thrust during static tests and the run tank's fuel mass. RMC will also **activate the DC motors needed** for drogue parachute ejection, and heat a piece of nichrome wire in order for the main parachute to be deployed.

For this project we used an STM32L476RG and the STM32CUBEMx to setup the peripherals and FREERTOS.
It contains 2 main threads: One responsible to trigger the events and the other thread that reads our sensors' data.
